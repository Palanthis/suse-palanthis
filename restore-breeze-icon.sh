#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis
# Website 	: 	http://gitlab.com/Palanthis
# License	:	BSD 2-Clause
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# This is undoes the script breeze-fix, as sometimes updates
# will fail, if the real breeze icon theme is not found.

# Remove simlink
sudo rm -rf /usr/share/icons/breeze

# Rename Breeze-light back to Breeze
sudo mv /usr/share/icons/breeze-light /usr/share/icons/breeze
