#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis
# Website 	: 	http://gitlab.com/Palanthis
# License	:	BSD 2-Clause
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install packages
sudo zypper -n install dolphin-plugins kgpg neofetch ruby3.0-rubygem-lolcat kwalletmanager5
sudo zypper -n install kvantum-qt5 kvantum-themes kvantum-manager pam_kwallet samba plank
sudo rpm -i rpms/smb4k-3.0.7-5.3.x86_64.rpm

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo [ -d ~/.local/share/templates ] || sudo mkdir -p ~/.local/share/templates
[ -d ~/Wallpapers ] || mkdir -p ~/Wallpapers
sudo tar xzf tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf tarballs/templates.tar.gz -C ~/.local/share/ --overwrite
sudo tar xzf tarballs/smb.conf.tar.gz -C /etc/samba/ --overwrite
sudo tar xzf tarballs/adapta-home.tar.gz -C ~/ --overwrite
sudo tar xzf tarballs/share.tar.gz -C /usr/share/ --overwrite
sudo tar xzf tarballs/wallpapers.tar.gz -C ~/Wallpapers/ --overwrite
sudo tar xzf tarballs/logo.tar.gz -C /usr/share/icons/ --overwrite


echo 'neofetch | lolcat' >> ~/.bashrc




