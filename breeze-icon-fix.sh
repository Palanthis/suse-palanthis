#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis
# Website 	: 	http://gitlab.com/Palanthis
# License	:	BSD 2-Clause
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# This is a fix for dark themes in Plasma. Plasma falls back to breeze icon theme by default.
# When you are using a dark theme, and Plasma cannot find an icon, it will use one from breeze.
# These are black icons designed for light themes. This script will rename breeze and then
# soft-link breeze-dark to breeze, so that Plasma falls back to white icons, which are visible
# when using dark themes.

# Rename breeze
sudo mv /usr/share/icons/breeze /usr/share/icons/breeze-light

# Link breeze-dark to breeze
sudo ln -sf /usr/share/icons/breeze-dark /usr/share/icons/breeze
